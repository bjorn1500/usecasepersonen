﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UseCasePersonen.Models
{
    class Person
    {
        public int? PersonId { get; private set; }
        public string FirstName { get; private set; }
        public string Name { get; private set; }
        public string TelNumber { get; private set; }
        public string CellNumber { get; private set; }
        public int? BirthYear { get; private set; }
        public int? Children { get; private set; }
        public Address Address { get; set; }

        public void FillPerson(int? personId, string firstName, string name, string telNumber, string cellNumber, int? birthYear, int? children)
        {
            PersonId = personId;
            if (!string.IsNullOrEmpty(firstName))
            {
                FirstName = firstName;
            }
            else
            {
                throw new Exception("tis kappot");
            }

            if (!string.IsNullOrEmpty(name))
            {
                this.Name = name;
            }
            else
            {
                throw new Exception("tis kappot");
            }

            TelNumber = telNumber;
            CellNumber = cellNumber;
            BirthYear = birthYear;
            Children = children ?? 0;

        }
    }
}
