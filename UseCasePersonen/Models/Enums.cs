﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UseCasePersonen.Models
{
    enum Fields
    {
        Naam = 1,
        Voornaam = 2,
        TelNummer = 3,
        GSMNummer = 4,
        GeboorteJaar = 5,
        AantalKinderen = 6,
        Straat = 7,
        HuisNummer = 8,
        Gemeente = 9,
        Postcode = 10,
        Land = 11
    }
}
