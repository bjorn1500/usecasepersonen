﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UseCasePersonen.Models
{
    class Address
    {
        public int? AddressId { get; set; }
        public string StreetName { get; private set; }
        public string HouseNumber { get; private set; }
        public string PostalCode { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }

        public void FillAddress(int? addressId, string streetName, string houseNumber, string postalCode, string city, string country)
        {
            AddressId = addressId;

            if (!string.IsNullOrEmpty(streetName))
            {
                StreetName = streetName;
            }
            else
            {
                throw new Exception("Street name not filled in");
            }

            if (!string.IsNullOrEmpty(houseNumber))
            {
                HouseNumber = houseNumber;
            }
            else
            {
                throw new Exception("House number not filled in");
            }

            if (!string.IsNullOrEmpty(postalCode))
            {
                PostalCode = postalCode;
            }
            else
            {
                throw new Exception("Postalcode not filled in");
            }

            if (!string.IsNullOrEmpty(city))
            {
                City = city;
            }
            else
            {
                throw new Exception("city not filled in");
            }

            if (!string.IsNullOrEmpty(country))
            {
                Country = country;
            }
            else
            {
                throw new Exception("Country not filled in");
            }

        }
    }
}
