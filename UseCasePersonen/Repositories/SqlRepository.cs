﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using UseCasePersonen.Models;

namespace UseCasePersonen.Repositories
{
    class SqlRepository
    {
        const string _connectionString = "Data Source=localhost; Initial Catalog=UseCasePersonen; User ID=sa; Password=SQL12345";
        public List<Person> GetAllPersons()
        {
            var result = new List<Person>();
            string sql = "SELECT p.PersonId, p.Voornaam, p.Naam, p.TelNummer, " +
                         "p.GSMNummer, p.GeboorteJaar, p.AantalKinderen, " +
                         "a.AdresId, a.Straat, a.HuisNummer, a.Postcode, " +
                         "a.Gemeente, a.Land " +
                         "FROM Person p " +
                         "INNER JOIN Adres a ON p.AdresId = a.AdresId";

            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand(sql, connection))
            {
                connection.Open();

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    int personId = reader.GetInt32(reader.GetOrdinal("PersonId"));
                    string firstName = reader["Voornaam"].ToString();
                    var name = reader["Naam"].ToString();
                    var telNumber = reader["TelNummer"].ToString();
                    var cellNumber = reader["GSMNummer"].ToString();
                    var birthYear = reader.GetInt32(reader.GetOrdinal("GeboorteJaar"));
                    var children = reader.GetInt32(reader.GetOrdinal("AantalKinderen"));
                    var addressId = reader.GetInt32(reader.GetOrdinal("AdresId"));
                    var streetName = reader["Straat"].ToString();
                    var houseNumber = reader["HuisNummer"].ToString();
                    var postalCode = reader["Postcode"].ToString();
                    var city = reader["Gemeente"].ToString();
                    var country = reader["Land"].ToString();

                    var person = new Person();
                    person.FillPerson(personId, firstName, name, telNumber, cellNumber, birthYear, children);

                    var address = new Address();
                    address.FillAddress(addressId, streetName, houseNumber, postalCode, city, country);
                    person.Address = address;

                    result.Add(person);
                }
            }

            return result;
        }

        public int CreateAddress(Address address)
        {
            var sql = "Insert into Adres (Straat, HuisNummer, Gemeente, Postcode, Land) " +
                "VALUES (@Straat, @HuisNummer, @Gemeente, @Postcode, @Land) " +
                "SELECT @@IDENTITY";

            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@Straat", address.StreetName);
                command.Parameters.AddWithValue("@HuisNummer", address.HouseNumber);
                command.Parameters.AddWithValue("@Gemeente", address.City);
                command.Parameters.AddWithValue("@Postcode", address.PostalCode);
                command.Parameters.AddWithValue("@Land", address.Country);

                connection.Open();

                decimal id = (decimal)command.ExecuteScalar();

                return Convert.ToInt32(id);
            }
        }

        public void CreatePerson(Person person)
        {
            var sql = "Insert into Person (Naam, Voornaam, TelNummer, GSMNummer, GeboorteJaar, AantalKinderen, AdresId) " +
                "values (@Naam, @Voornaam, @TelNummer, @GSMNummer, @GeboorteJaar, @AantalKinderen, @AdresId)";

            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@Naam", person.Name);
                command.Parameters.AddWithValue("@Voornaam", person.FirstName);
                if (!string.IsNullOrEmpty(person.TelNumber))
                {
                    command.Parameters.AddWithValue("@TelNummer", person.TelNumber);
                }
                else
                {
                    command.Parameters.AddWithValue("@TelNummer", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(person.TelNumber))
                {
                    command.Parameters.AddWithValue("@GSMNummer", person.CellNumber);
                }
                else
                {
                    command.Parameters.AddWithValue("@GSMNummer", DBNull.Value);
                }
                if (person.BirthYear.HasValue)
                {
                    command.Parameters.AddWithValue("@GeboorteJaar", person.BirthYear);
                }
                else
                {
                    command.Parameters.AddWithValue("@GeboorteJaar", DBNull.Value);
                }
                if (person.Children.HasValue)
                {
                    command.Parameters.AddWithValue("@AantalKinderen", person.Children);
                }
                else
                {
                    command.Parameters.AddWithValue("@AantalKinderen", DBNull.Value);
                }
                command.Parameters.AddWithValue("@AdresId", person.Address.AddressId);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        public void UpdateField(Fields field, string value, int personId)
        {
            switch (field)
            {
                case Fields.Naam:
                case Fields.Voornaam:
                case Fields.TelNummer:
                case Fields.GSMNummer:
                case Fields.GeboorteJaar:
                case Fields.AantalKinderen:
                    UpdatePerson(field, value, personId);
                    break;
                case Fields.Straat:
                case Fields.HuisNummer:
                case Fields.Gemeente:
                case Fields.Postcode:
                case Fields.Land:
                    UpdateAddress(field, value, personId);
                    break;
                default:
                    break;
            }
        }

        private void UpdateAddress(Fields field, string value, int personId)
        {
            string columnName = Enum.GetName(typeof(Fields), field);
            var sql = $"UPDATE a SET a.{columnName} = @Value from Adres a " +
                      "INNER JOIN Person p ON a.AdresId = p.AdresId " +
                      "where p.PersonId = @personID";

            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand(sql, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@Value", value);
                command.Parameters.AddWithValue("@personID", personId);

                command.ExecuteNonQuery();
            }
        }

        private void UpdatePerson(Fields field, string value, int personId)
        {
            string columnName = Enum.GetName(typeof(Fields), field);

            var sql = $"UPDATE Person SET {columnName} = @Value where PersonId = @personID";

            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand(sql, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@Value", value);
                command.Parameters.AddWithValue("@personID", personId);

                command.ExecuteNonQuery();
            }

        }

        public void DeletePerson(int id)
        {
            var sql = "DeletePerson";
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand(sql, connection))
            {
                connection.Open();

                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@PersonID", id);

                command.ExecuteNonQuery();
            }
        }
    }
}
