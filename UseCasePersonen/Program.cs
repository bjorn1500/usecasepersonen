﻿using System;
using UseCasePersonen.Repositories;
using UseCasePersonen.Models;

namespace UseCasePersonen
{
    class Program
    {
        static bool stop = false;
        static SqlRepository repo = new SqlRepository();
        static void Main(string[] args)
        {
            while (!stop)
            {
                Console.WriteLine("What do u want to do!!!!!!!!!")
                Console.WriteLine("1. List contacts");
                Console.WriteLine("2. Create contact");
                Console.WriteLine("3. Edit contact");
                Console.WriteLine("4. Delete contact");
                Console.WriteLine("5. Stop");

                var success = int.TryParse(Console.ReadLine(), out int result);

                if (success)
                {
                    switch (result)
                    {
                        case 1:
                            ListContact();
                            break;
                        case 2:
                            CreateContact();
                            break;
                        case 3:
                            EditContact();
                            break;
                        case 4:
                            DeleteContact();
                            break;
                        case 5:
                            stop = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private static void DeleteContact()
        {
            Console.WriteLine("Geef de ID van de persoon die je wil verwijderen( id 0 om terug te keren):");
            var successId = int.TryParse(Console.ReadLine(), out int id);

            if (successId)
            {
                if (id == 0)
                    return;

                repo.DeletePerson(id);
            }
            else
            {
                Console.WriteLine("Foute ingave! probeer opnieuw.");
                DeleteContact();
                return;
            }

        }

        private static void EditContact()
        {
            Console.WriteLine("Geef de ID van de persoon die je wil aanpassen( id 0 om terug te keren):");
            var successId = int.TryParse(Console.ReadLine(), out int id);

            if (successId)
            {
                if (id == 0)
                {
                    return;
                }

                Console.WriteLine("Gelieve te kiezen wat je wil aanpassen:");
                Console.WriteLine("1.Naam\n2.Voornaam\n3.Telefoon nummer\n4.GSM nummer\n5.Geboortejaar\n6.Aantal kinderen\n7.Straat\n8.Huisnummer\n9.Gemeente\n10.Postcode\n11.Land");

                var successField = int.TryParse(Console.ReadLine(), out int fieldNumber);
                if (!successField || !(fieldNumber < 11 && fieldNumber > 0))
                {
                    Console.WriteLine("Foute ingave! probeer opnieuw.");
                    EditContact();
                    return;
                }

                Fields? field = (Fields?)fieldNumber;
                if (!field.HasValue)
                {
                    Console.WriteLine("Foute ingave! probeer opnieuw.");
                    EditContact();
                    return;
                }

                Console.WriteLine("Geef de nieuwe waarde: ");
                string value = Console.ReadLine();

                if (field == Fields.GeboorteJaar || field == Fields.AantalKinderen)
                {
                    if (!int.TryParse(value, out int result))
                    {
                        Console.WriteLine("Foute ingave! probeer opnieuw.");
                        EditContact();
                        return;
                    }
                }

                repo.UpdateField(field.Value, value, id);
            }
            else
            {
                Console.WriteLine("Foute ingave! probeer opnieuw.");
                EditContact();
                return;
            }

        }

        static void ListContact()
        {
            var persons = repo.GetAllPersons();

            Console.WriteLine("ID;Naam;Voornaam;Telefoon nummer;GSM nummer;Geboortejaar;Aantal kinderen; Straat;Huisnummer;Gemeente;Postcode;Land");
            foreach (var person in persons)
            {
                Console.WriteLine($"{person.PersonId};{person.Name};{person.FirstName};{person.TelNumber};{person.CellNumber};{person.BirthYear};{person.Children};{person.Address.StreetName};{person.Address.HouseNumber};{person.Address.City};{person.Address.PostalCode};{person.Address.Country}");
            }
        }

        static void CreateContact()
        {
            string name, firstName, telNumber, celNumber, streetName, houseNumber, city, postalCode, country;
            int? birthYear, children;

            Console.WriteLine("Geef de gegevens in het volgende formaat");
            Console.WriteLine("Naam;Voornaam;Telefoon nummer;GSM nummer;Geboortejaar;Aantal kinderen; Straat;Huisnummer;Gemeente;Postcode;Land");
            var result = Console.ReadLine();
            var resultArray = result.Split(";");
            if (resultArray.Length == 11)
            {
                try
                {
                    name = resultArray[0];
                    firstName = resultArray[1];
                    telNumber = resultArray[2];
                    celNumber = resultArray[3];
                    birthYear = Convert.ToInt32(resultArray[4]);
                    children = Convert.ToInt32(resultArray[5]);
                    streetName = resultArray[6];
                    houseNumber = resultArray[7];
                    city = resultArray[8];
                    postalCode = resultArray[9];
                    country = resultArray[10];

                    var person = new Person();
                    person.FillPerson(null, firstName, name, telNumber, celNumber, birthYear, children);
                    var address = new Address();

                    address.FillAddress(null, streetName, houseNumber, postalCode, city, country);

                    address.AddressId = repo.CreateAddress(address);
                    person.Address = address;
                    repo.CreatePerson(person);
                }
                catch (Exception e)
                {
                    Console.WriteLine("the format is incorrect.");
                    CreateContact();
                }

            }
            else
            {
                Console.WriteLine("the format is incorrect.");
                CreateContact();
            }


        }
    }
}
